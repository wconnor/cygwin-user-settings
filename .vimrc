call pathogen#infect()
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
syntax enable 
set mouse=a
set background=dark
colorscheme solarized
filetype plugin indent on
